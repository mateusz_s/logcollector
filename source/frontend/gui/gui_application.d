module gui;

import tkd.tkdapplication;

class GuiApplication : TkdApplication
{
    private void exitCommand(CommandArgs args)
    {
        this.exit();
    }

    private Frame createWidgetPane()
    {
        auto widgetPane = new Frame();

        auto entryLabelFrame = new LabelFrame(widgetPane, "łat")
            .pack(10, 0, GeometrySide.top, GeometryFill.both, AnchorPosition.center, true);

        return widgetPane;
    }

    override protected void initInterface() // Initialise user interface.
    {
        this.mainWindow.setTitle("Logger collector");
        this.mainWindow.setMinSize(480, 640);

        auto noteBook = new NoteBook();

        auto widgetPane = this.createWidgetPane();

        noteBook
            .addTab("Widgets", widgetPane)
            .pack(10, 0, GeometrySide.top, GeometryFill.both, AnchorPosition.center, true);
    
    }
}
