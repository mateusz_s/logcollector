module termui;

import core.thread;
import std.stdio;

class TermUI
{
public:
    void run()
    {
        writeln("Started application");
        while (true)
        {
            Thread.sleep(dur!("msecs")(50));
        }
    }
}