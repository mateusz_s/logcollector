module application;

import server;

class Application(Type)
{
public:
    this()
    {
        app_ = new Type();
        loggingServer_ = new Server(1234);
    }

    void start()
    {
        loggingServer_.start();
        app_.run();
    }

private:
    Server loggingServer_;
    Type app_;
}
