module server;

import core.thread;
import std.array;
import std.socket;
import std.stdio;

class Server
{
public:
    this(ushort port) {
        port_ = port;
        socket_ = new TcpSocket();
    }

    void start()
    {
        socket_.bind(new InternetAddress(port_));
        socket_.listen(10);
        writeln("Socket started on port: ", port_);

        auto serverThread = new Thread({
            auto logFile = File("targetlog", "a");
            logFile.writeln("Test");
            logFile.close();
            long bytesRead;
            ubyte[1] buff;
            ubyte[] data;
            while (true)
            {
                auto currSock = socket_.accept();
                while ((bytesRead = currSock.receive(buff)) > 0) 
                {
                    data ~= buff;
                }
                   // currSock.send(buff);
                auto log = File("targetlog", "a");
                log.writeln(cast(string)(data));
                writeln(cast(string)(data));
                currSock.close();
                //buff.clear();
                data = [];

                Thread.sleep(dur!("msecs")(50));
            }
        });
        serverThread.isDaemon(true);
        serverThread.start();
    }

    void stop()
    {

    }
private:
    ushort port_;
    TcpSocket socket_;
    File logFile_;
}
