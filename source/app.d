// Copyright (C) 2017  Mateusz Stadnik
//
// This file is part of Log Collector.
//
// Log Collector is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Log Collector is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Log Collector.  If not, see <http://www.gnu.org/licenses/>.
//

import std.getopt;
import std.stdio;

import application;
import gui;
import termui;


void main(string[] args)
{
	bool noGui;
	auto helpInformation = getopt(
		args,
		"no-gui|t", "run in terminal mode", &noGui);

	if (helpInformation.helpWanted)
	{
		defaultGetoptPrinter("Some information about the program.",
	    	helpInformation.options);
	    return;
	}

	if (!noGui)
	{
		writeln("GUI");
        auto app = new Application!(GuiApplication)(); // Create the application.
        app.start(); // Run the application.
	}
	else
	{
		writeln("Term");

        auto app = new Application!(TermUI)();
        app.start();
	}

}

